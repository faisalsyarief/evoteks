/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : codingtest

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 27/10/2022 20:48:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for candidate_lists
-- ----------------------------
DROP TABLE IF EXISTS `candidate_lists`;
CREATE TABLE `candidate_lists`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `experience` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `top_5_Skills` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `candidate_list_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `candidate_list_phone_unique`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of candidate_lists
-- ----------------------------
INSERT INTO `candidate_lists` VALUES (4, 'tes', 'tes', '1991-01-20', 'tes', 'tes', 'tes', 'tes', 'tes@gmail.com', '0986325834567', 'tes', '2022-10-27 12:26:54', '2022-10-27 12:26:54');
INSERT INTO `candidate_lists` VALUES (5, 'smith1', 'UGM Yogyakarta', '1991-01-19', '5 Year', 'CEO', 'Senior PHP Developer', 'Laravel, Mysql, PostgreSQL, Codeigniter, Java', 'smith5@gmail.com', '0851234567893', '-', '2022-10-27 12:26:54', '2022-10-27 12:26:54');
INSERT INTO `candidate_lists` VALUES (6, 'smithhhh', 'UGM Yogyakartaaa', '1991-01-20', '5 Yearrrr', 'CEOooo', 'Senior PHP Developerrrr', 'Laravel, Mysql, PostgreSQL, Codeigniter, Javaaaa', 'smith2222@gmail.com', '0851234567899', '-', '2022-10-27 12:43:56', '2022-10-27 12:43:56');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_10_27_041554_create_candidate_list_table', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
INSERT INTO `personal_access_tokens` VALUES (1, 'App\\Models\\User', 6, 'auth_token', 'd101d8955fef37e1edb6ff2bd8538202b6f03eeeef80738e32c610936cb9bccb', '[\"*\"]', NULL, '2022-10-27 03:42:44', '2022-10-27 03:42:44');
INSERT INTO `personal_access_tokens` VALUES (2, 'App\\Models\\User', 7, 'auth_token', '257e4f4e8de4bd0b78de777c296c1e1607776c9ed513f646148c49b79c7cb89a', '[\"*\"]', NULL, '2022-10-27 03:43:48', '2022-10-27 03:43:48');
INSERT INTO `personal_access_tokens` VALUES (3, 'App\\Models\\User', 8, 'auth_token', '49868a85884f410af1758dc3b0c086ebbaaac0fe8b0997ae80639f65bffca1e4', '[\"*\"]', NULL, '2022-10-27 03:46:18', '2022-10-27 03:46:18');
INSERT INTO `personal_access_tokens` VALUES (4, 'App\\Models\\User', 9, 'auth_token', 'f6bfc759241750b458efcc4c60377405a2fb9c7b415fa22b5af4d9b099739302', '[\"*\"]', NULL, '2022-10-27 03:51:52', '2022-10-27 03:51:52');
INSERT INTO `personal_access_tokens` VALUES (5, 'App\\Models\\User', 10, 'auth_token', '4996161beeeaab3c8cadb91417ae4686be4dd2ddf88d07a3c39320406039fb71', '[\"*\"]', NULL, '2022-10-27 03:52:15', '2022-10-27 03:52:15');
INSERT INTO `personal_access_tokens` VALUES (6, 'App\\Models\\User', 9, 'auth_token', '86b72bc7d5e0f6fc186e01b6f6233f03925d7ab61137978bff76f4138d5a3fe7', '[\"*\"]', NULL, '2022-10-27 03:54:37', '2022-10-27 03:54:37');
INSERT INTO `personal_access_tokens` VALUES (7, 'App\\Models\\User', 9, 'auth_token', 'dc0f5306e716e0854e7b49b4623f9e6dea7d852eaaa1f33c7fc9db9513d8e3a1', '[\"*\"]', NULL, '2022-10-27 04:02:44', '2022-10-27 04:02:44');
INSERT INTO `personal_access_tokens` VALUES (8, 'App\\Models\\User', 9, 'auth_token', '1458a842d34dce3bdbcc3e47bdb78ded11a452d5e5fca02c4a26e4a3660b3e6c', '[\"*\"]', NULL, '2022-10-27 04:13:35', '2022-10-27 04:13:35');
INSERT INTO `personal_access_tokens` VALUES (9, 'App\\Models\\User', 9, 'auth_token', 'a8553fbb811acc5b0d77a619902fa706982f3c7ae2ea81aa7d45020da7e94255', '[\"*\"]', '2022-10-27 13:01:49', '2022-10-27 04:45:07', '2022-10-27 13:01:49');
INSERT INTO `personal_access_tokens` VALUES (10, 'App\\Models\\User', 9, 'auth_token', 'f2a6e17fb1323abfb31e56a1a827384afe2fe6f8e4297c0de00c9f02f447c8e9', '[\"*\"]', NULL, '2022-10-27 09:41:40', '2022-10-27 09:41:40');
INSERT INTO `personal_access_tokens` VALUES (11, 'App\\Models\\User', 9, 'auth_token', 'f0536a57ac2e5fde4c1ea26264fc0e261dbd7ef9297ddf7c088906cea6aed4af', '[\"*\"]', NULL, '2022-10-27 10:00:18', '2022-10-27 10:00:18');
INSERT INTO `personal_access_tokens` VALUES (12, 'App\\Models\\User', 9, 'auth_token', 'a1f7202fdcfee7421f7e114f1523cf5f18befe64ee8c0c3f90aad42331e44d5e', '[\"*\"]', '2022-10-27 12:38:50', '2022-10-27 10:13:46', '2022-10-27 12:38:50');
INSERT INTO `personal_access_tokens` VALUES (13, 'App\\Models\\User', 9, 'auth_token', '42e1f68431c35d40de46c794623c4585994db9055df5ff3784b74f2b5a1c94cb', '[\"*\"]', '2022-10-27 12:26:54', '2022-10-27 12:15:25', '2022-10-27 12:26:54');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (9, 'Mr. John', 'mrjohn@gmail.com', NULL, '$2y$10$//WjZqUdLC.NcLZpOUgEbOn/3s9erqeSMlIMDkU4b3QF0h0cZvUEK', NULL, '2022-10-27 03:51:52', '2022-10-27 03:51:52', 'Senior HRD');
INSERT INTO `users` VALUES (10, 'Mrs. Lee', 'mrslee@gmail.com', NULL, '$2y$10$n3AmrssBZF1LPPigVScN7OZ5I93j7W5KsiphRPJp4oXSTLavKLYpm', NULL, '2022-10-27 03:52:15', '2022-10-27 03:52:15', 'HRD');

SET FOREIGN_KEY_CHECKS = 1;
