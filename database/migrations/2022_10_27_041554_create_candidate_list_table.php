<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_lists', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('education');
            $table->date('birthday');
            $table->string('experience');
            $table->string('last_position');
            $table->string('applied_position');
            $table->text('top_5_Skills');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->text('resume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_lists');
    }
}
