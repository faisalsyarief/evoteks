<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//API Register User
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//API Login User
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

//Protecting Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function (Request $request) {
        return auth()->user();
    });

    // Route::resource('/candidate', App\Http\Controllers\API\CandidateAPI::class);
    Route::apiResource('candidate', App\Http\Controllers\API\CandidateAPI::class);
    // Route::delete('/candidate/{id}',[App\Http\Controllers\API\CandidateAPI::class, 'destroy']);

    // API Logout
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
});
