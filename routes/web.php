<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/data_user', [App\Http\Controllers\HomeController::class, 'view_data'])->name('view_data');
Route::post('/simpan_data_user',[App\Http\Controllers\HomeController::class, 'save_data']);
Route::get('/edit_data_user/{id}',[App\Http\Controllers\HomeController::class, 'update_data']);
Route::post('/update_data_user',[App\Http\Controllers\HomeController::class, 'update']);
Route::get('/hapus_data_user/{id}',[App\Http\Controllers\HomeController::class, 'delete_data']);

Route::get('/data_candidate', [App\Http\Controllers\CandidateController::class, 'view_data'])->name('view_data');
Route::post('/simpan_data_candidate',[App\Http\Controllers\CandidateController::class, 'save_data']);
Route::get('/edit_data_candidate/{id}',[App\Http\Controllers\CandidateController::class, 'update_data']);
Route::post('/update_data_candidate',[App\Http\Controllers\CandidateController::class, 'update']);
Route::get('/hapus_data_candidate/{id}',[App\Http\Controllers\CandidateController::class, 'delete_data']);