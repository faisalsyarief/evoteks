<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\CandidateList;
use Exception;
use Illuminate\Support\Facades\Http;

class CandidateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view_data()
    {
        $candidate_list = DB::table('candidate_lists')->get();
        return view('candidate.data_candidate_list', ['data_candidate_list' => $candidate_list]);
    }

    public function save_data(Request $request)
    {
        DB::table('candidate_lists')->insert([
            'name' =>  $request->name,
            'education' =>  $request->education,
            'birthday' =>  $request->birthday,
            'experience' =>  $request->experience,
            'last_position' =>  $request->last_position,
            'applied_position' =>  $request->applied_position,
            'top_5_Skills' =>  $request->top_5_Skills,
            'email' =>  $request->email,
            'phone' =>  $request->phone,
            'resume' =>  $request->resume
        ]);

        return redirect('candidate.data_candidate_list');
    }

    public function update_data($id)
    {
        $candidate_list = DB::table('candidate_lists')->where('id', $id)->get();
        return view('candidate.edit_data_candidate_list', ['data_candidate_list' => $candidate_list]);
    }

    public function update(Request $request)
    {
        DB::table('candidate_lists')->where('id', $request->id)->update([
            'name' =>  $request->name,
            'education' =>  $request->education,
            'birthday' =>  $request->birthday,
            'experience' =>  $request->experience,
            'last_position' =>  $request->last_position,
            'applied_position' =>  $request->applied_position,
            'top_5_Skills' =>  $request->top_5_Skills,
            'email' =>  $request->email,
            'phone' =>  $request->phone,
            'resume' =>  $request->resume
        ]);
        return redirect('candidate.data_candidate_list');
    }

    public function delete_data($id)
    {
        DB::table('candidate_lists')->where('id', $id)->delete();
        return redirect('candidate.data_candidate_list');
    }
}
