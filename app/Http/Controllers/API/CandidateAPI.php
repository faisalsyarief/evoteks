<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Models\CandidateList;
use App\Http\Resources\CandidateListResource;
use Illuminate\Support\Facades\DB;

/**
 * @OAS\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      type="http",
 *      scheme="bearer"
 * )
 */

class CandidateAPI extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/candidate",
     *      operationId="getcandidateList",
     *      tags={"candidate"},
     *      summary="list candidate API",
     *      description="list candidate API",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     * 
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * 
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        $data = CandidateList::all();
        if (is_null($data)) {
            return response()->json('Data not found', 404);
        }
        return response()
            ->json(['message' => 'succes', 'data' => $data]);
    }

    /**
     * @OA\Post(
     *      path="/api/candidate",
     *      operationId="postcandidateList",
     *      tags={"candidate"},
     *      summary="add candidate API",
     *      description="Add candidate API",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(name="name", in="query", required=true, ),
     *      @OA\Parameter(name="education", in="query", required=true, ),
     *      @OA\Parameter(name="birthday", in="query", required=true, ),
     *      @OA\Parameter(name="experience", in="query", required=true, ),
     *      @OA\Parameter(name="last_position", in="query", required=true, ),
     *      @OA\Parameter(name="applied_position", in="query", required=true, ),
     *      @OA\Parameter(name="top_5_Skills", in="query", required=true, ),
     *      @OA\Parameter(name="email", in="query", required=true, ),
     *      @OA\Parameter(name="phone", in="query", required=true, ),
     *      @OA\Parameter(name="resume", in="query", required=true, ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     * 
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * 
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'education' => 'required|string|max:255',
            'birthday' => 'required|date',
            'experience' => 'required',
            'last_position' => 'required',
            'applied_position' => 'required',
            'top_5_Skills' => 'required',
            'email' => 'required|email',
            'phone' => 'required|min:11|numeric',
            'resume' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $data = CandidateList::create([
            'name' =>  $request->name,
            'education' =>  $request->education,
            'birthday' =>  $request->birthday,
            'experience' =>  $request->experience,
            'last_position' =>  $request->last_position,
            'applied_position' =>  $request->applied_position,
            'top_5_Skills' =>  $request->top_5_Skills,
            'email' =>  $request->email,
            'phone' =>  $request->phone,
            'resume' =>  $request->resume
        ]);

        return response()->json(['data created successfully.', new CandidateListResource($data)]);
    }

    /**
     * @OA\Get(
     *      path="/api/candidate/{id}",
     *      operationId="getidcandidateList",
     *      tags={"candidate"},
     *      summary="get candidate by ID API",
     *      description="Get candidate by ID API",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *          name="id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     * 
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * 
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function show($id)
    {
        $data = CandidateList::find($id);
        if (is_null($data)) {
            return response()->json('Data not found', 404);
        }

        return response()
            ->json(['message' => 'succes', 'data' => $data]);
    }

    /**
     * @OA\Put(
     *      path="/api/candidate/{id}",
     *      operationId="putcandidateList",
     *      tags={"candidate"},
     *      summary="put candidate API",
     *      description="Put candidate API",
     *      @OA\Parameter(
     *          name="id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(name="name", in="query", required=true, ),
     *      @OA\Parameter(name="name", in="query", required=true, ),
     *      @OA\Parameter(name="education", in="query", required=true, ),
     *      @OA\Parameter(name="birthday", in="query", required=true, ),
     *      @OA\Parameter(name="experience", in="query", required=true, ),
     *      @OA\Parameter(name="last_position", in="query", required=true, ),
     *      @OA\Parameter(name="applied_position", in="query", required=true, ),
     *      @OA\Parameter(name="top_5_Skills", in="query", required=true, ),
     *      @OA\Parameter(name="email", in="query", required=true, ),
     *      @OA\Parameter(name="phone", in="query", required=true, ),
     *      @OA\Parameter(name="resume", in="query", required=true, ),
     * 
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */

    public function update($id, Request $request, CandidateList $data)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'education' => 'required|string|max:255',
            'birthday' => 'required|date',
            'experience' => 'required',
            'last_position' => 'required',
            'applied_position' => 'required',
            'top_5_Skills' => 'required',
            'email' => 'required|email',
            'phone' => 'required|min:11|numeric',
            'resume' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $data->id =  $id;
        $data->name =  $request->name;
        $data->education =  $request->education;
        $data->birthday =  $request->birthday;
        $data->experience =  $request->experience;
        $data->last_position =  $request->last_position;
        $data->applied_position =  $request->applied_position;
        $data->top_5_Skills =  $request->top_5_Skills;
        $data->email =  $request->email;
        $data->phone =  $request->phone;
        $data->resume =  $request->resume;

        DB::table('candidate_lists')->where('id', $data->id)->update([
            'name' =>  $request->name,
            'education' =>  $request->education,
            'birthday' =>  $request->birthday,
            'experience' =>  $request->experience,
            'last_position' =>  $request->last_position,
            'applied_position' =>  $request->applied_position,
            'top_5_Skills' =>  $request->top_5_Skills,
            'email' =>  $request->email,
            'phone' =>  $request->phone,
            'resume' =>  $request->resume
        ]);

        return response()
            ->json(['message' => 'berhasil merubah data ' . $data->id, 'data' => $data]);
    }

    /**
     * @OA\Delete(
     *      path="/api/candidate/{id}",
     *      operationId="deletecandidateList",
     *      tags={"candidate"},
     *      summary="delete candidate API",
     *      description="Delete candidate API",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *          name="id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     * 
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * 
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function destroy($id, CandidateList $data)
    {
        if (DB::table('candidate_lists')->where('id', $id)->delete()) {
            return response()->json('Data ' . $id . ' deleted successfully');
        }
        return response()->json('gagal hapus data', 404);
    }
}
