<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function view_data()
    {
        $users = DB::table('users')->get();
        return view('data_user', ['data_user' => $users]);
    }

    public function save_data(Request $request)
    {
        DB::table('users')->insert([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),

        ]);

        return redirect('/data_user');
    }

    public function update_data($id)
    {
        $users = DB::table('users')->where('id', $id)->get();
        return view('edit_data_user', ['data_user' => $users]);
    }

    public function update(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return redirect('/data_user');
    }

    public function delete_data($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect('/data_user');
    }
}
