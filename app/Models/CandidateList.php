<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class CandidateList extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['name', 'education', 'birthday', 'experience', 'last_position', 'applied_position', 'top_5_Skills', 'email', 'phone', 'resume'];
}
