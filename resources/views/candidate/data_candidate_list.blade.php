@extends('layouts.app')

@section('content')
    <link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css" rel="stylesheet">

    <div class="container">
        <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">{{ __('Data Candidate') }}</div>
                <div class="card-body" style="overflow-x:auto;">
                    <input type="hidden" id="level" value="{{ Auth::user()->level }}">
                    <table id="table_view" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>name</th>
                                <th>education</th>
                                <th>birthday</th>
                                <th>experience</th>
                                <th>last_position</th>
                                <th>applied_position</th>
                                <th>top_5_Skills</th>
                                <th>email</th>
                                <th>phone</th>
                                <th>resume</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_candidate_list as $data)
                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->education }}</td>
                                    <td>{{ $data->birthday }}</td>
                                    <td>{{ $data->experience }}</td>
                                    <td>{{ $data->last_position }}</td>
                                    <td>{{ $data->applied_position }}</td>
                                    <td>{{ $data->top_5_Skills }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->phone }}</td>
                                    <td>{{ $data->resume }}</td>
                                    <td>
                                        @if (Auth::user()->level == 'Senior HRD')
                                            <a href="/edit_data_candidate/{{ $data->id }}">Edit</a>
                                            |
                                            <a href="/hapus_data_candidate/{{ $data->id }}">Hapus</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/simpan_data_candidate" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label for="education">Education</label>
                            <input type="text" class="form-control" name="education">
                        </div>
                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            <input type="date" class="form-control" name="birthday">
                        </div>
                        <div class="form-group">
                            <label for="experience">Experience</label>
                            <input type="text" class="form-control" name="experience">
                        </div>
                        <div class="form-group">
                            <label for="last_position">Last Position</label>
                            <input type="text" class="form-control" name="last_position">
                        </div>
                        <div class="form-group">
                            <label for="applied_position">Applied Position</label>
                            <input type="text" class="form-control" name="applied_position">
                        </div>
                        <div class="form-group">
                            <label for="top_5_Skills">Top 5 Skills</label>
                            <input type="text" class="form-control" name="top_5_Skills">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="resume">Resume</label>
                            <input type="text" class="form-control" name="resume">
                        </div>
                        <input type="submit" class="btn btn-primary" value="Simpan Data">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            if (document.getElementById("level").value == 'Senior HRD') {
                $('#table_view').DataTable({

                    dom: 'Bfrtip',
                    buttons: [
                        '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> + Tambah </button>',
                        'print'
                    ]
                });
            } else {
                $('#table_view').DataTable({
                    dom: 'Bfrtip',
                });
            }
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
@endsection
