@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row justify-content-center"> --}}
        <div class="card">
            <div class="card-header">{{ __('Data Candidate') }}</div>
            <div class="card-body">
                @foreach ($data_candidate_list as $data)
                    <form action="/update_data_candidate" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="nama" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label for="education">Education</label>
                            <input type="text" class="form-control" name="education" value="{{ $data->education }}">
                        </div>
                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            <input type="text" class="form-control" name="birthday" value="{{ $data->birthday }}">
                        </div>
                        <div class="form-group">
                            <label for="experience">Experience</label>
                            <input type="text" class="form-control" name="experience" value="{{ $data->experience }}">
                        </div>
                        <div class="form-group">
                            <label for="last_position">Last Position</label>
                            <input type="text" class="form-control" name="last_position"
                                value="{{ $data->last_position }}">
                        </div>
                        <div class="form-group">
                            <label for="applied_position">Applied Position</label>
                            <input type="text" class="form-control" name="applied_position"
                                value="{{ $data->applied_position }}">
                        </div>
                        <div class="form-group">
                            <label for="top_5_Skills">Top 5 Skills</label>
                            <input type="text" class="form-control" name="top_5_Skills"
                                value="{{ $data->top_5_Skills }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ $data->email }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{ $data->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="resume">Resume</label>
                            <input type="text" class="form-control" name="resume" value="{{ $data->resume }}">
                        </div>
                        <input type="submit" class="btn btn-primary" value="Simpan Data">
                    </form>
                @endforeach
            </div>
        </div>
        {{-- </div> --}}
    </div>
@endsection
