@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row justify-content-center"> --}}
        <div class="card">
            <div class="card-header">{{ __('Data User') }}</div>
            <div class="card-body">
                @foreach ($data_user as $data)
                    <form action="/update_data_user" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="nama" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ $data->email }}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" value="{{ $data->password }}">
                        </div>
                        <input type="submit" class="btn btn-primary" value="Simpan Data">
                    </form>
                @endforeach
            </div>
        </div>
        {{-- </div> --}}
    </div>
@endsection
